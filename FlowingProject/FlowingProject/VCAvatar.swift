//
//  VCAvatar.swift
//  FlowingProject
//
//  Created by Tomate Albertini on 18/05/16.
//  Copyright © 2016 Ronaldo Albertini. All rights reserved.
//

import UIKit

class VCAvatar: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView:UITableView!
    
    let cellIdentifier = "TCAvatar"
    var ismarked = [false,false,false]
    
    var avatars : Array<Avatar> = Array<Avatar>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        
        Services.callAvatarList(Dictionary<String,AnyObject>(), completionHandler: { jsonresponse in
            
            for avatar in jsonresponse! {
                self.avatars.append(Avatar.fromDictionary(Dictionary<String,AnyObject>(dictionaryLiteral: avatar)))
            }
        })
        
        
        tableView.registerNib(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
         
    
        let a = Avatar()
        a.AvatarId = 1
        a.AvatarBio = "Biografia do Avatar"
        a.AvatarName = "ALOY"
        a.isSelected = false
        a.AvatarPicturePath = "01"
        
        self.avatars.append(a)
        
        let b = Avatar()
        b.AvatarId = 2
        b.AvatarBio = "Biografia do Avatar"
        b.AvatarName = "FLOU"
        b.isSelected = false
        b.AvatarPicturePath = "02"
        
        self.avatars.append(b)
        
        let c = Avatar()
        c.AvatarId = 3
        c.AvatarBio = "Biografia do Avatar"
        c.AvatarName = "RAGI"
        c.isSelected = false
        c.AvatarPicturePath = "03"
        
        self.avatars.append(c)
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.navigationBar.topItem?.title = "Coach"
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .Done, target: nil, action: nil)
        
    }
    
    
    
    //MARK: Table View DataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.avatars.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let avat = avatars[indexPath.row]
        
        let cell: TCAvatar = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! TCAvatar
        
        cell.delegate = self
        cell.imgAvatar.image =   UIImage(named: avat.AvatarPicturePath!)
        
        cell.setChecked(ismarked[indexPath.row])
        cell.bCheckMark.tag = indexPath.row
        cell.bCheckMark.addTarget(self, action: #selector(VCAvatar.checkmarkTapped), forControlEvents: .TouchUpInside)
        
        cell.lTitle.text = avat.AvatarName!
        cell.lDescription.text = avat.AvatarBio!
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 145
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    

   func checkmarkTapped(sender : UIButton) {
    
        ismarked = [false,false,false]
    
        ismarked[ sender.tag ] = true
        tableView.reloadData()
        print(ismarked)
        
    }

}
