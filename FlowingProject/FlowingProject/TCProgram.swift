//
//  TCProgram.swift
//  FlowingProject
//
//  Created by Tomate Albertini on 18/05/16.
//  Copyright © 2016 Ronaldo Albertini. All rights reserved.
//

import UIKit

class TCProgram: UITableViewCell {

    @IBOutlet weak var lProgramName: UILabel!
    @IBOutlet weak var lProgramShortDescription: UILabel!
    @IBOutlet weak var lPartnerName: UILabel!
    @IBOutlet weak var lPartnerSignature: UILabel!
    @IBOutlet weak var imgPartnerProfilePicture: UIImageView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var vPartner: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.vPartner.backgroundColor = UIColor(red: 6.0/255.0, green: 116.0/255.0, blue: 138.0/255.0, alpha: 1.0)
        self.imgPartnerProfilePicture.layer.cornerRadius = 32
        self.imgPartnerProfilePicture.layer.masksToBounds = true;
        
        self.lPartnerName.sizeToFit()
        self.lPartnerSignature.sizeToFit()
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
