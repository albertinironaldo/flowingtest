//
//  Services.swift
//  FlowingProject
//
//  Created by Tomate Albertini on 18/05/16.
//  Copyright © 2016 Ronaldo Albertini. All rights reserved.
//

import Foundation
import Alamofire

class Services:NSObject {
    
    static let BASE_URL_TESTE = "http://dev.flowing.com.br/"
    static var TOKEN = ""
    
    
    static func makePostRequest(endpoint:String, headers:Dictionary<String,String>,params:Dictionary<String,AnyObject>, completion:(Dictionary<String, AnyObject>)->Void) {
        
        Alamofire.request(.POST,
            BASE_URL_TESTE + endpoint,
            parameters:params,
            headers:headers )
            
            //.validate(statusCode: 200..<300)
            .response { request, response, data, error in
                
                //print(request)
                print(response)
                
                do {
                    let jsonResponse = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as? Dictionary<String, AnyObject>
                    print(jsonResponse)
                   // if response?.statusCode == 200 {
                        completion(jsonResponse!)
                   // }
                }
                catch let error as NSError {
                    print(error)
                }
        }
    }
    
    
    static func makeGetRequest(endpoint:String, headers:Dictionary<String,String>,params:Dictionary<String,AnyObject>, completion:(Dictionary<String, AnyObject>)->Void) {
        
        Alamofire.request(.GET, BASE_URL_TESTE + endpoint, parameters: params, headers:headers )
            
            //.validate(statusCode: 200..<300)
            .response { request, response, data, error in
                
                print(request)
                print(response)
                if response?.statusCode == 200 {
                    
                    do {
                        let jsonResponse = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as? Dictionary<String, AnyObject>
                        print(jsonResponse)
                        completion(jsonResponse!)
                        
                    }
                    catch let error as NSError {
                        
                        print(error)
                    }
                } else {
                    print(error)
                }
        }
    }

    
    static func callAvatarList(params:Dictionary<String,AnyObject>,completionHandler : (Dictionary<String,AnyObject>?) -> Void) {
        
        let endpoint = "v1/configurations/avatars"
        
        let headers = ["Authorization": "bearer" + self.TOKEN]
        
        
        self.makeGetRequest(endpoint, headers: headers, params:Dictionary<String,AnyObject>(),
                            
                            completion:{jsonResponse in completionHandler(jsonResponse) }
        )
        
    }
    
    static func callLogin(params:Dictionary<String,AnyObject>,completionHandler : (Dictionary<String,AnyObject>?) -> Void) {
        
        let endpoint = "account/gettoken"
        
        let headers = ["Encoding": "x-www-form-encoded"]

        
        print(params["username"]!)
        print(params["password"]!)
        
        self.makePostRequest(endpoint, headers: headers, params:
            ["username"  : params["username"]!,
             "password"  : params["password"]!,
             "grant_type":        "password"],
                             
                             completion:{jsonResponse in
                                
                                if let value = jsonResponse["access_token"] as? String {
                                    self.TOKEN = value
                                }
                                
                                
                                completionHandler(jsonResponse)
                                
        })
    }
    
    
    
    
    
}