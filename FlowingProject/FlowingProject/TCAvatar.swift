//
//  TCAvatar.swift
//  FlowingProject
//
//  Created by Tomate Albertini on 18/05/16.
//  Copyright © 2016 Ronaldo Albertini. All rights reserved.
//

import UIKit

class TCAvatar: UITableViewCell {

    @IBOutlet weak var bCheckMark:UIButton!
    @IBOutlet weak var imgAvatar:UIImageView!
    @IBOutlet weak var lTitle:UILabel!
    @IBOutlet weak var lDescription:UILabel!
    
    var isChecked:Bool?
    
    var delegate:VCAvatar?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setChecked(checked : Bool ){
        
        self.isChecked = checked
        
        if checked {
            bCheckMark.setImage(UIImage(named:"check"), forState: .Normal)
            bCheckMark.layer.borderWidth = 0
        } else {

            bCheckMark.setImage(UIImage(named:"circle"), forState: .Normal)
            bCheckMark.backgroundColor = UIColor.clearColor()
            bCheckMark.layer.cornerRadius = 15
            bCheckMark.layer.borderWidth = 1
            bCheckMark.layer.borderColor = UIColor.blackColor().CGColor
            
        }
    }
    
}
