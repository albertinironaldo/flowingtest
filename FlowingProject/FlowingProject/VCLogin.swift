//
//  VCLogin.swift
//  FlowingProject
//
//  Created by Tomate Albertini on 18/05/16.
//  Copyright © 2016 Ronaldo Albertini. All rights reserved.
//

import UIKit

class VCLogin: UIViewController {

    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var bLogin:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        bLogin.backgroundColor = UIColor(red: 6.0/255.0, green: 116.0/255.0, blue: 138.0/255.0, alpha: 1.0)
        bLogin.tintColor = UIColor.whiteColor()
        bLogin.layer.cornerRadius = 4.0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.navigationBar.topItem?.title = "Login"
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .Done, target: nil, action: nil)
    }
    
    
    @IBAction func entrarClick(sender:UIButton) {
        
       // self.navigationController?.pushViewController(VCMenu(nibName: "VCMenu",bundle: nil), animated: true)
       // return
        
        if self.validate() {
            
            var params:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
            
            params["username"] = self.txtEmail.text!
            params["password"] = self.txtPassword.text!
            
                Services.callLogin(params,completionHandler: {jsonResponse in
                
                    print(jsonResponse)
                    
                    if Services.TOKEN != "" {
                       
                        self.navigationController?.pushViewController(VCMenu(nibName: "VCMenu",bundle: nil), animated: true)
                        
                    } else {
                        
                        if let value = jsonResponse!["error_description"] as? String {
                            
                            self.navigationController?.pushViewController(VCMenu(nibName: "VCMenu",bundle: nil), animated: true)
                            self.showAlert(value)
                        }
                        
                    }
                })
  
        }
    }

    func validate() ->Bool {
        
        if self.txtEmail.text! == "" {
            self.showAlert("Favor preencher o Email")
        } else if self.txtPassword.text! == "" {
            self.showAlert("Favor preencher a Senha")
        } else {
            return true
        }
        
       return false
    }
    
    func showAlert(message:String) {
        
        let alertController =  UIAlertController(title: "Atenção", message: message, preferredStyle: .Alert)
        let alertAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(alertAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}
