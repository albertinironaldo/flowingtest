//
//  VCMenu.swift
//  FlowingProject
//
//  Created by Tomate Albertini on 18/05/16.
//  Copyright © 2016 Ronaldo Albertini. All rights reserved.
//

import UIKit

class VCMenu: UIViewController {

    
    @IBOutlet weak var bPrograms:UIButton!
    @IBOutlet weak var bCoach:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bPrograms.backgroundColor = UIColor(red: 6.0/255.0, green: 116.0/255.0, blue: 138.0/255.0, alpha: 1.0)
        bPrograms.tintColor = UIColor.whiteColor()
        bPrograms.layer.cornerRadius = 4.0
        
        bCoach.backgroundColor = UIColor(red: 6.0/255.0, green: 116.0/255.0, blue: 138.0/255.0, alpha: 1.0)
        bCoach.tintColor = UIColor.whiteColor()
        bCoach.layer.cornerRadius = 4.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.navigationBar.topItem?.title = "Menu"
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .Done, target: nil, action: nil)
    }
    
    @IBAction func bCoachTap(sender: UIButton) {
        self.navigationController?.pushViewController(VCAvatar(nibName: "VCAvatar",bundle: nil), animated: true)
    }
    
    @IBAction func bProgramsTap(sender: UIButton) { 
        self.navigationController?.pushViewController(VCPrograms(nibName: "VCPrograms",bundle: nil), animated: true)
    }

}
