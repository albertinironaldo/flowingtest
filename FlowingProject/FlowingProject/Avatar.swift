//
//  Avatar.swift
//  FlowingProject
//
//  Created by Tomate Albertini on 18/05/16.
//  Copyright © 2016 Ronaldo Albertini. All rights reserved.
//

import UIKit

class Avatar: NSObject {

    var AvatarId:Int?
    var AvatarName:String?
    var AvatarBio:String?
    var AvatarPicturePath:String?
    var isSelected:Bool?
    
    static func fromDictionary(json:Dictionary<String,AnyObject>) -> Avatar {
        
        let avatar:Avatar = Avatar()
        
        if let value = json["AvatarId"] as? Int {
            avatar.AvatarId = value
        }
        
        if let value = json["AvatarName"] as? String {
            avatar.AvatarName = value
        }
        
        if let value = json["AvatarBio"] as? String {
            avatar.AvatarBio = value
        }
        
        if let value = json["AvatarPicturePath"] as? String {
            avatar.AvatarPicturePath = value
        }
        
        if let value = json["isSelected"] as? Bool {
            avatar.isSelected = value
        }
        
        return avatar
    }
    
}
