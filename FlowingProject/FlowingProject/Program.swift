//
//  Program.swift
//  FlowingProject
//
//  Created by Tomate Albertini on 18/05/16.
//  Copyright © 2016 Ronaldo Albertini. All rights reserved.
//

import UIKit

class Program: NSObject {

    var UserId:Int?
    var ProgramId:Int?
    var UserName:String?
    var ProgramName:String?
    var ProgramTypeId:String?
    var ProgramType:String?
    var ProgramDescription:String?
    var ProgramShortDescription:String?
    var ProgramImage_square:String?
    
    static func fromDictionary(json:Dictionary<String,AnyObject>) -> Program {
        
        let program:Program = Program()
        
        if let value = json["UserId"] as? Int {
            program.UserId = value
        }
        
        if let value = json["ProgramId"] as? Int {
            program.ProgramId = value
        }
        
        if let value = json["UserName"] as? String {
            program.UserName = value
        }
        
        if let value = json["ProgramName"] as? String {
            program.ProgramName = value
        }
        
        if let value = json["ProgramTypeId"] as? String {
            program.ProgramTypeId = value
        }
        
        if let value = json["ProgramType"] as? String {
            program.ProgramType = value
        }
        
        if let value = json["ProgramDescription"] as? String {
            program.ProgramDescription = value
        }
        
        if let value = json["ProgramShortDescription"] as? String {
            program.ProgramShortDescription = value
        }
        
        if let value = json["ProgramImage_square"] as? String {
            program.ProgramImage_square = value
        }
        
        return program
    }
}
