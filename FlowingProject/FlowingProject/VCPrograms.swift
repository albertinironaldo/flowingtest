//
//  VCPrograms.swift
//  FlowingProject
//
//  Created by Tomate Albertini on 18/05/16.
//  Copyright © 2016 Ronaldo Albertini. All rights reserved.
//

import UIKit

class VCPrograms: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView:UITableView!
    
    let cellIdentifier = "TCProgram"
  
    var programs : Array<Program> = Array<Program>()
    
    let dictA = ["UserId"                   :1,
                 "ProgramId"                :1,
                 "UserName"                 :"Dr. Garcia Lopes",
                 "ProgramName"              :"ESTRESSE",
                 "ProgramTypeId"            :"1",
                 "ProgramType"              :"Otorrino, FMUSP",
                 "ProgramDescription"       :"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloreLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloreLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore",
                 "ProgramShortDescription"  :"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore",
                 "ProgramImage_square"      :"person1"]
    
    let dictB = ["UserId"                   :2,
                 "ProgramId"                :2,
                 "UserName"                 :"Dr. João Marcos",
                 "ProgramName"              :"FOBIA",
                 "ProgramTypeId"            :"2",
                 "ProgramType"              :"Anestesista, FMUSP",
                 "ProgramDescription"       :"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloreLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloreLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore",
                 "ProgramShortDescription"  :"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore",
                 "ProgramImage_square"      :"person3"]
    
    let dictC = ["UserId"                   :3,
                 "ProgramId"                :3,
                 "UserName"                 :"Dr. Telma Soares",
                 "ProgramName"              :"DEPRESSÃO",
                 "ProgramTypeId"            :"3",
                 "ProgramType"              :"Psicanalista, FMUSP",
                 "ProgramDescription"       :"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloreLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloreLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore",
                 "ProgramShortDescription"  :"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore",
                 "ProgramImage_square"      :"person2"]
    

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.registerNib(UINib(nibName: cellIdentifier,    bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        self.programs.append(Program.fromDictionary(dictA))
        self.programs.append(Program.fromDictionary(dictB))
        self.programs.append(Program.fromDictionary(dictC))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.navigationBar.topItem?.title = "Programas"
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .Done, target: nil, action: nil)
    }


    //MARK: Table View DataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.programs.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let prog = programs[indexPath.row]
        
        let cell: TCProgram = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! TCProgram
        
        cell.imgBackground.image =   UIImage(named: "food0\(indexPath.row+1)")
        cell.lPartnerName.text = prog.UserName
        cell.lProgramName.text = prog.ProgramName
        cell.lProgramShortDescription.text = prog.ProgramDescription
        cell.imgPartnerProfilePicture.image = UIImage(named: prog.ProgramImage_square!)
        cell.lPartnerSignature.text = prog.ProgramType        
 
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 250
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
}
